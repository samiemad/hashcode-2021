#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<ll, ll> pll;
typedef vector<ll> vll;

ll D, I, S, V, F;
map<string, int> id;
vector<pair<int,int> > adj[100010];
pair< pair <int, int> ,int > EdgeL[100010];
vector<string> Names;
vector<int> incoming[100010];
vector<vll> route;

int freq[100010];

vector<vll> ans;

int main(){
	//srand(time(NULL));
	//string output = argv[1];
	//ios_base::sync_with_stdio(false);
    ifstream fin ("a.txt");
    ofstream fout ("a.out");

    fin >>D>>I>>S>>V>>F;
    for (int i=0;i<S;i++){
        int a,b;
        string nm;
        int len;

        fin >>a>>b;
        fin >>nm;
        fin >>len;

        Names.push_back(nm);
        id[nm]=i+1;
        EdgeL[i+1]={{a,b},len};
        incoming[b].push_back(id[nm]);
        adj[a].push_back({b,len});
    }
    for(int i=0; i<V; ++i){
		ll n;
		fin>>n;
		route.push_back({});
		while(n--){
			string s;
			fin>>s;
            freq[id[s]]++;
			route.back().push_back(id[s]);
		}
	}
	vector<int> ans1;

    for (int i=0;i<I;i++){
        ans.push_back({});
        ans1.push_back(i);
        for (int l=0;l<incoming[i].size();l++){
            if (freq[incoming[i][l]]){
                ans.back().push_back(incoming[i][l]);
            }
        }
        if (ans.back().empty()){
            ans.pop_back();
            ans1.pop_back();
        }
    }

    fout <<ans.size()<<endl;
    for (int i=0;i<ans.size();i++){
        fout <<ans1[i]<<endl;
        fout <<ans[i].size()<<endl;
        for (auto it:ans[i]){
            fout <<Names[it-1]<<" "<<1<<endl;
        }
    }

	return 0;
}
