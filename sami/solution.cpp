#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<u_int8_t, ll> pll;
typedef vector<ll> vll;
#define mp(x, y) make_pair(x, y)

const ll MX = (1ll<<31)-1;
// const ll MX = 10000001;
const ll MX1 = 500001;
const ll MD = 8;
ll B;
vector<u_int8_t> v(MX, 100);
// set<pll> s;
vector<tuple<u_int32_t,u_int32_t,char>> h(MX);

ll fact(ll x){
	if(x>30 || x<=2){
		return 0;
	}
	ll ans=x;
	while(--x){
		ans*=x;
		if(ans>=MX){
			return 0;
		}
	}
	return ans;
}

ll powr(ll x, ll p){
	if(p>32){
		return 0;
	}
	ll ans =x;
	while(--p){
		ans*=x;
		if(ans>=MX){
			return 0;
		}
	}
	return ans;
}

ll sqt(ll x){
	ll s = sqrt(x)+1e-6;
	if(s*s==x){
		return s;
	}
	return 0;
}

bool ok(ll nx){
	return nx>0 && nx<MX;
}

int main(int argc, char **argv){
	ios_base::sync_with_stdio(false);

	cout<<"BASE=";
	cin>>B;
	priority_queue<pll, vector<pll>, greater<pll>> q;

	ll b = B;
	for(int i=1; i<10; ++i){
		q.push({i,b});
		// s.insert({i,b});
		v[b] = i;
		h[b] = {b,0,'`'};
		b = 10*b +B;
		if(!ok(b)) break;
	}

	b=0;
	while(!q.empty()) {
		ll x, y, nx, ny;
		tie(y, x) = q.top();
		q.pop();
		if(y!=v[x] || y>MD) continue;

		if(++b %10000 == 0) cerr<<b/10000<<"0k\t| "<<x<<": "<<y<<"\t(q:"<<q.size()<<
		// ")\t(s:"<<s.size()<<
		")\n";

		ny = y;
		nx = fact(x); 
		// cerr<<">> "<<nx<<"\n";
		if(ok(nx) && ny<v[nx]){
			// s.erase({v[nx],nx});
			// s.insert({ny,nx});
			v[nx] = ny;
			h[nx] = {x, 0, '!'};
			q.push({ny,nx});
		}

		nx = sqt(x); ny = y;
		// cerr<<">> "<<nx<<"\n";
		if(ok(nx) && ny<v[nx]){
			// s.erase({v[nx],nx});
			// s.insert({ny,nx});
			v[nx] = ny;
			h[nx] = {x, 0, 'v'};
			q.push({ny,nx});
		}

		if(y==MD) continue;

		for(int i=1; i<MX1; ++i){
		// for(auto p:s){
		// 	auto i = p.second;
			if(v[i]>y) continue;
			ny = y+v[i];
			if(ny>MD) continue;
			nx = x+i;
			// cerr<<">> "<<nx<<"\n";
			if(ok(nx) && ny<v[nx]){
				// s.erase({v[nx],nx});
				// s.insert({ny,nx});
				v[nx] = ny;
				h[nx] = {x, i, '+'};
				q.push({ny,nx});
			}
			nx = abs(x-i);
			// cerr<<">> "<<nx<<"\n";
			if(ok(nx) && ny<v[nx]){
				// s.erase({v[nx],nx});
				// s.insert({ny,nx});
				v[nx] = ny;
				h[nx] = {x, i, '-'};
				q.push({ny,nx});
			}
			nx = x*i;
			// cerr<<">> "<<nx<<"\n";
			if(ok(nx) && ny<v[nx]){
				// s.erase({v[nx],nx});
				// s.insert({ny,nx});
				v[nx] = ny;
				h[nx] = {x, i, '*'};
				q.push({ny,nx});
			}
			nx = x%i==0 ? x/i : 0;
			// cerr<<">> "<<nx<<"\n";
			if(ok(nx) && ny<v[nx]){
				// s.erase({v[nx],nx});
				// s.insert({ny,nx});
				v[nx] = ny;
				h[nx] = {x, i, '/'};
				q.push({ny,nx});
			}
			nx = i%x==0 ? i/x : 0;
			// cerr<<">> "<<nx<<"\n";
			if(ok(nx) && ny<v[nx]){
				// s.erase({v[nx],nx});
				// s.insert({ny,nx});
				v[nx] = ny;
				h[nx] = {i, x, '/'};
				q.push({ny,nx});
			}
			nx = powr(x,i);
			// cerr<<">> "<<nx<<"\n";
			if(ok(nx) && ny<v[nx]){
				// s.erase({v[nx],nx});
				// s.insert({ny,nx});
				v[nx] = ny;
				h[nx] = {x, i, '^'};
				q.push({ny,nx});
			}
			nx = powr(i,x);
			// cerr<<">> "<<nx<<"\n";
			if(ok(nx) && ny<v[nx]){
				// s.erase({v[nx],nx});
				// s.insert({ny,nx});
				v[nx] = ny;
				h[nx] = {i, x, '^'};
				q.push({ny,nx});
			}
		}
	}
	cout<<"Done\n";

	while(cin>>b){
		ll x,y; char o;
		tie(x,y,o) = h[b];
		cout<<b<<": "<<ll(v[b])<<"\t"<<x<<o<<y<<"\n";
	}

	return 0;
}
